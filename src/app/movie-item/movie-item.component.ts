import { Component, Input } from '@angular/core';
import { Movie } from '../movie-list/movie-list.component';

@Component({
  selector: 'app-movie-item',
  standalone: true,
  imports: [],
  templateUrl: './movie-item.component.html',
  styleUrl: './movie-item.component.css'
})
export class MovieItemComponent {

  @Input()
  movieDetails?: Movie

  isFavourite = false

  addToFavourite() {
    this.isFavourite = true;
    // alert(`${this.movieDetails.title} a été ajouté aux favoris`);
  }
}
