import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title', // app c'est le nom du module
  standalone: true,
  imports: [],
  template: `
    <h1>Title: {{ titleContent }}</h1>
  `,
  styles: [`
    h1 {
      color: red;
    }
  `]
})
export class TitleComponent {

  @Input()
  titleContent: string = "";
}
