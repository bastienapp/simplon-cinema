import { Component } from '@angular/core';
import { MovieItemComponent } from '../movie-item/movie-item.component';
import { HttpClient } from '@angular/common/http';

export type Movie = {
  id: number,
  title: string,
  releaseDate: string,
  director: string,
  description: string,
  poster: string,
}

@Component({
  selector: 'app-movie-list',
  standalone: true,
  imports: [MovieItemComponent],
  templateUrl: './movie-list.component.html',
  styleUrl: './movie-list.component.css'
})
export class MovieListComponent {

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    fetch('/assets/movies.json')
      .then((data) => data.json())
      .then((response: Movie[]) => this.movieList = response)
      .catch((error) => console.error(error))

    // avec HttpClient
    /*
    this.http.get<Movie[]>('/assets/movies.json')
      .subscribe((response: Movie[]) => this.movieList = response)
    */
  }

  movieList: Movie[] = []
}
