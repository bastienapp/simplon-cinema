import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { TitleComponent } from './shared/title/title.component';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { MovieListComponent } from './movie-list/movie-list.component';

@Component({
  selector: 'main-component',
  standalone: true,
  imports: [
    RouterOutlet,
    TitleComponent,
    MovieListComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Simplon Ciné';
  description = 'lorem ipsum'
}
